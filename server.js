/* eslint-disable no-console */
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const { PORT } = require("./config");

const routes = require("./routes");
const handleErrors = require("./middlewares/errorHandler");
const { connect } = require("./utils/dbConnection");
const { logger } = require("./utils/logger");

const app = express();
const port = PORT;

app.use(cors());

app.use(bodyParser.urlencoded({ extended: true }));

app.use(bodyParser.json());

// connect to database
connect((err, client) => {
    if (err) {
        console.log(err);
    }
});

// Routes
app.use("/", routes);

app.use(handleErrors);

app.listen(port, () => {
    logger.info(`Server listening at http://localhost:${port}`);
});
