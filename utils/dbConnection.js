const { MongoClient } = require("mongodb");
const { MONGODB_URL } = require("../config");
const url = MONGODB_URL;
const client = new MongoClient(url);
const dbName = "dinotesDB";

let db;

exports.connect = (callback) => {
    client.connect((err, client) => {
        db = client.db(dbName);
        return callback(err, client);
    });
};

exports.getDb = () => {
    return db;
};
