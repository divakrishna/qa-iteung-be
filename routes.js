/* eslint-disable no-console */
const express = require('express');
const passport = require('passport');

const router = express.Router();

const { addNote, getAllNotes, getNote, updateNote, deleteNote } = require('./handler');

const { addQA, getAllQAs, getQA, updateQA, deleteQA, register, login } = require('./handler');

require('./utils/auth');

// notes
router.post('/note', passport.authenticate('jwt', { session: false }), addNote);

router.get('/notes', passport.authenticate('jwt', { session: false }), getAllNotes);

router.get('/note/:id', passport.authenticate('jwt', { session: false }), getNote);

router.put('/note/:id', passport.authenticate('jwt', { session: false }), updateNote);

router.delete('/note/:id', passport.authenticate('jwt', { session: false }), deleteNote);

// qas
router.post('/qa', passport.authenticate('jwt', { session: false }), addQA);

router.get('/qas', passport.authenticate('jwt', { session: false }), getAllQAs);

router.get('/qa/:id', passport.authenticate('jwt', { session: false }), getQA);

router.put('/qa/:id', passport.authenticate('jwt', { session: false }), updateQA);

router.delete('/qa/:id', passport.authenticate('jwt', { session: false }), deleteQA);

// user
router.post('/register', register);

router.post('/login', login);

module.exports = router;
